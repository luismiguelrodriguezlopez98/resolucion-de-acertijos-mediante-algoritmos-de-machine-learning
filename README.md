# **Resolución de acertijos mediante algoritmos de Machine Learning**

## **Introducción**

El planteamiento sobre el cual se va a desarrollar el proyecto de clase es el siguiente, ¿De qué forma se pueden usar algoritmos de machine learning para la resolución de acertijos que tiene más de una forma de solucionarse tomando en cuenta las reglas de los mismos?

Partiendo de la premisa anterior se plantea un proyecto que permite utilizar diferentes tipos de algoritmos machine learning tales como: BFS, DFS y Random Forest Classifier. Y que, partiendo de condiciones ya establecidas, sean usados para construir una inteligencia artificial que sea capaz de resolver acertijos como el juego de las N reinas, y los misioneros & caníbales, de tal manera que al implementarle un numero de N pasos, genere una serie de resultados para las posibles jugadas que se podrían realizar como solución a los anteriores juegos mencionados. Siendo el objetivo principal el mostrar el estado (Ganó, Perdió, sigue jugando) en el que se encuentra el acertijo, luego de haberle aplicado uno de los métodos de búsqueda de los algoritmos mencionados anteriormente, y de esta manera comparar mediante un riguroso análisis, cual es el algoritmo más útil para la resolución de los acertijos.

## **Misioneros y caníbales**

El juego plantea un acertijo donde se tienen tres misioneros y tres caníbales del mismo lado del mapa. El propósito del juego es que el jugador debe cruzar todos los misioneros y caníbales a través del rio en una barca hacía el otro lado. Cabe resaltar que la barca no podrá cruzar el rio si está vacía, y que en la barca solo se puede llevar un máximo de dos personas y que siempre debe haber una mayoría numérica de misioneros junto a los caníbales, si esto no se cumple entonces los caníbales se comerán a los misioneros danto por terminado el juego.

## **Problemas de las “N” reinas**

El problema de las N reinas es un pasatiempo que consiste en poner N reinas en el tablero de ajedrez de dimensiones NxN. Fue propuesto por el ajedrecista alemán Max Bezzel en 1848. En el juego del ajedrez la reina amenaza a aquellas piezas que se encuentren en su misma fila, columna o diagonal. El juego de las N reinas consiste en poner sobre un tablero de ajedrez N reinas sin que estas se amenacen entre ellas.

## **Proceso y método**

Reuniendo los conocimientos obtenidos mediante la investigación de los temas tratados en el plan de estudios, y gracias a la asesoría impartida por el docente a cargo de la materia, se realizó un esquema inicial

*   *Entorno:* El proyecto se ha realizado en Colaboratory, utilizando el lenguaje de programación Python.

*   *Intervenciones:* Se implementaron los algoritmos BFS y DFS para construir un árbol de búsqueda que pueda encontrar las N soluciones para cada uno de los acertijos. Además, se utiliza el algoritmo RFC (Random Forest Classifier) de tal manera que se pueda trabajar con un dataset, donde tengamos un número de jugadas “x” para terminar el juego, “x + 1” serán nuestro número de columnas en el dataset ya que al final tendremos la clase o estado del juego (Ganó, En juego, Perdió). El número de filas será determinado por todas las posibles combinaciones de movimientos que se pueden tomar.

*   *Análisis estadístico:* El análisis a realizar está dividido para cada uno de los algoritmos utilizados, en el caso de implementar el algoritmo BFS y DFS, se compara el menor coste de tiempo que tiene cada uno de estos. En el caso del algoritmo Random Forest Classifier se busca evaluar y análizar el "accuracy" o precisión, dependiendo del número de estimadores y la profundidad implementada en la búsqueda de soluciones.
